# Redvision

## Recommended ffmpeg commands

```bash
ffmpeg -f image2 -framerate 30 -i frame_%d.png -c:v libvpx-vp9 -b:v 0 \
    -pix_fmt yuv420p -crf 30 -cpu-used 1 -tile-columns 2 -threads 8 -row-mt 1 \
    out.webm
```

* crf: lower gets better quality but larger size. (range between: -1, 63)
* cpu-used: higher gets faster encoding but less efficient compression.

Some helping references:

- <https://gist.github.com/Vestride/278e13915894821e1d6f>
- <https://sites.google.com/a/webmproject.org/wiki/ffmpeg/vp9-encoding-guide>
- <https://video.stackexchange.com/questions/24443/webm-blocky-in-initial-frames-terrible-for-looping>
