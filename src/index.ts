// import * as archiver from "archiver";
import { saveAs } from "file-saver";
import * as JSZip from "jszip";
import * as p5 from "p5";

// import * as fs from "fs";

import "./style.css";

const CANVAS_WIDTH = 600;
const CANVAS_HEIGHT = 800;
const CANVAS_ID = "cucumber";
// const CANVAS_WIDTH = 1920;
// const CANVAS_HEIGHT = 1080;
const FPS = 30;
const REC_DURATION = 60; // in seconds

const BACKGROUND = 0;
const LINES = 80;
const POINTS = 50;
const MAX_AMPLITUDE = 100;
const POW_AMPLITUDE = Math.log(MAX_AMPLITUDE) / Math.log(2);

let recording = false;
const frames = new JSZip();
let frameCount = 0;
const noiseSeeds: number[] = [];
let time = 0;
let canvas: HTMLCanvasElement;

function delay(ms: number): Promise<void> {
    return new Promise((res) => setTimeout(res, ms));
}

function makeNoise(amplitude: number) {
    return Math.round(Math.random() * amplitude) - amplitude / 2;
}

function setup(p: p5): void {
    p.createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT).id("cucumber");
    p.frameRate(FPS);
    p.strokeWeight(1);
    // p.noLoop();

    canvas = document.getElementById(CANVAS_ID) as HTMLCanvasElement;
    if (recording) {
        canvas.style.display = "none";
    }

    for (let j = 0; j < LINES; j++) {
        noiseSeeds.push(Math.random() * 10000);
    }
}

function createLines(p: p5): p5.Vector[][] {
    const lines: p5.Vector[][] = [];

    for (let j = 0; j < LINES; j++) {
        const line: p5.Vector[] = [];
        const baseY = (j + 1) * (CANVAS_HEIGHT / (LINES + 1));
        // line.push(p.createVector(0, baseY));

        for (let i = 0; i < POINTS; i++) {
            // const x = (i + 1) * (CANVAS_WIDTH / (POINTS + 1));
            const x = i === 0 ? 0 : i * (CANVAS_WIDTH / (POINTS - 1));

            // const perc = j / LINES * 100;
            // let distModifier;
            // if (perc < 100) {
            //     distModifier = 30 / 100 * (CANVAS_WIDTH / 2);
            //     // distModifier = (CANVAS_WIDTH) / 100 * 30;
            // } else {
            //     distModifier = 0;
            // }

            const distToMiddle = Math.abs(x - CANVAS_WIDTH / 2);

            let amplitude = Math.pow(
                2,
                POW_AMPLITUDE - distToMiddle
                    / (CANVAS_WIDTH / 2) * POW_AMPLITUDE,
            );

            if (amplitude > MAX_AMPLITUDE) {
                amplitude = MAX_AMPLITUDE;
            }

            const xStep = 0.5;
            const xProgression = p.map(x, 0, CANVAS_WIDTH, 0, POINTS * xStep);
            // console.log(x, xProgression);

            // const y = x !== POINTS ? baseY - p.random(0, amplitude) : baseY;
            const y = x !== POINTS ? baseY - p.noise(time + xProgression) * amplitude : baseY;
            line.push(p.createVector(x, y));
        }

        p.noiseSeed(noiseSeeds[j]);

        lines.push(line);
    }

    return lines;
}

async function draw(p: p5): Promise<void> {
    if (recording) {
        time += 1000 / FPS / 1000;
    } else {
        time += p.deltaTime / 1000;
    }

    const lines = createLines(p);

    p.background(BACKGROUND);

    // p.colorMode(p.RGB, 1);
    // p.fill(160, 0, 0);
    // p.fill(BACKGROUND);
    // p.stroke(160, 0, 0);

    let l = 0;
    for (const line of lines) {
        p.colorMode(p.HSL, 1);
        // const color = p.random();

        p.randomSeed(noiseSeeds[l]);
        const fillColor = p.noise(time + p.map(l, 0, LINES - 1, 0, 2) + p.random());
        const strokeColor = p.noise(time + p.map(l, 0, LINES - 1, 0, 2) + p.random());
        p.fill(fillColor, 0.7, 0.3);
        p.stroke(strokeColor, 0.9, 0.5);

        // p.fill(1, 0.7, 0.3);
        // p.stroke(1, 0.9, 0.5);
        // p.stroke(1, 0.7, 0.3);

        p.beginShape();
        for (let i = 0; i < POINTS; i++) {
            if (i === 0 || i === POINTS - 1) {
                p.curveVertex(line[i].x, line[i].y);
            }
            p.curveVertex(line[i].x, line[i].y);
        }
        p.endShape();

        // p.strokeWeight(4);
        // for (let i = 0; i < line.length; i++) {
        //     p.point(line[i].x, line[i].y);
        // }

        l++;
    }

    // p.stroke(1);
    // p.textSize(24);
    // p.text(`${time.toFixed(2)}, ${p.frameRate().toFixed(2)}`, 50, 50);

    if (recording) {
        frameCount++;

        const perc = Math.floor(frameCount / (FPS * REC_DURATION) * 100);
        if (perc > Math.floor((frameCount - 1) / (FPS * REC_DURATION) * 100)) {
            console.log(`recording: ${perc}% (${frameCount}/${REC_DURATION * FPS})`);
        }

        frames.file(`frame_${frameCount}.png`,
            canvas.toDataURL().replace(/^data:image\/(png|jpg);base64,/, ""), { base64: true });

        if (frameCount >= REC_DURATION * FPS) {
            console.log("zipping recording...");
            let zipPerc = 0;

            frames.generateAsync(
                { type: "blob" },
                (metadata) => {
                    const newZipPerc = Math.floor(metadata.percent)
                    if (zipPerc < newZipPerc) {
                        console.log(`zipping: ${newZipPerc}%`);
                        zipPerc = newZipPerc;
                    }
                },
            )
            .then((blob) => {
                console.log("saving recording...");
                saveAs(blob);
            })
            .catch((err) => {
                console.log(`couldn't zip the record: \`${err}\`.`);
            })
            .finally(() => {
                console.log("finished.");
                p.loop();
            });

            p.noLoop();
            recording = false;
            frameCount = 0;
        }
    }
}

function keyPress(p: p5): void {
    if (p.keyCode === p.RETURN) {
        recording = true;
    }
}

function sketch(p: p5): void {
    p.setup = () => { setup(p); };

    p.draw = () => { draw(p); };

    p.keyPressed = () => { keyPress(p); };
}

new p5(sketch);
