const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const CopyWebpackPlugin = require("copy-webpack-plugin");
// const HtmlWebpackIncludeAssetsPlugin = require("html-webpack-include-assets-plugin");
// const HtmlWebpackIncludeAssetsPlugin = require("html-webpack-tags-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
    node: { fs: "empty" },
    entry: "./src/index.ts",
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    devtool: "inline-source-map",
    devServer: {
        host: "0.0.0.0",
        contentBase: "./build",
        hot: true
    },
    plugins: [
        // new CopyWebpackPlugin([
        //     {
        //         from: "node_modules/ccapture.js/build/CCapture.all.min.js",
        //         to: "assets/js"
        //     }
        // ]),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: "src/index.html",
        }),
        // new HtmlWebpackIncludeAssetsPlugin({
        //     assets: ["assets/js/CCapture.all.min.js"],
        //     append: false
        // }),
        // new MiniCssExtractPlugin({
        //     filename: "[name].css",
        //     chunkFilename: "[id].css"
        // })
    ],
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "build")
    }
};
